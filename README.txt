-- SUMMARY --

Allows you to create menu items, upon entering into that show child items on the page. Example http://aeroflot.ru

-- REQUIREMENTS --

menu

-- RECOMMENDED --
menu_icons

-- INSTALLATION --

* Tick module on page modules

-- USE --

1. Create menu in Administer >> Site building >>
2. Create parent item with path <dummy>
3. Create children item with parent from previous item
4. Customize style using menu_dummy.css and menu_dummy.tpl.php

-- CONTACT --

Developer http://drupal.org/user/203339
This project has been sponsored by:
* JASMiND
  Is an independent professional consulting group with extensive background in telecommunications, IT, software development and implementation. Visit http://jmproduct.ru/en for more information.
* AEROFLOT
  Largest air carrier in Russia. Visit http://aeroflot.ru for more information.